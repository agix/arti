KEY:
- Not done
. Partially done
o complete
X Won't do.
? Did this happen?


MILESTONE 0: Build a circuit and use it
   o Read a directory from disk
   o Pick random relays
   o Construct a one-hop circuit with CREATE_FAST
   o Construct a one-hop circuit with ntor
   o Build relay cells
   o Construct a multihop circuit.
   o Open a stream
   o Make a request, get a response (no sendmes yet)

MILESTONE 1: Refactoring on above
   o Stop parameterizing all APIs on TLS type.
   o Combine "create circuit" and "create first hop"?
   o More types of circent for clarity.
   o Improve testing
   o Improve documentation
   o Make sure readme is right

MILESTONE 2: Working streams
   o Improve circuit API.
   o Get an initial stream API together.
   o Make end cells work, and get sent both ways.
   o Make SENDMEs work
   o Make authenticated sendmes work.
   o Make sendme authentication optional.
   o Solve SENDME bug with doubled flood/dl tests. (0.5) (40 min)
   o Circuit reactor shutdown needs to make stream reads fail (_/0.1) 5m to test
   o Circuit reactor shutdown needs to make stream writes fail (_/0.5) 30m
   o protocol failures on circuits/streams need to kill off the
     circuit. (_/0.5) 30m
   o Improve documentation
     o forbid missing private-docs everywhere (0.2) [2h 50m]
        o forbid missing private docs in tor-proto
   o Make sure readme is right (_/0) [10m]
   o Better solution for deadlock in lock on sendwindow.
   o Add a state for streams _we_ have closed where we haven't got an end
     from the other side.  Treat unexpected stream data as fatal.
     (Defend against DropMark attack from 25573) (1) (2:20h)

MILESTONE 3: Clean and tidy
   o Make sure that circuit Destroys are handled and sent correctly.
     Use ideas from circuit module. arti#5 (1) (2:30h)
   o Ed25519 needs to have an "identity" non-parsed version, maybe. arti#6
     (0.2) (1:20h)
   X Add other "partially-closed" states for streams and circuits? (1)
        - Audit to make sure Tor has no such things. (0.1)
   X Less copying, esp around Box<RawCellBody>.  Consider bytes crate. arti#7
     (1)
   o Use better types for cases when only a few messages are
     possible. arti#11 (0.3) (1:45h)
   o Audit all cell handling: unexpected cells mean kill the circuit arti#12
     [0.5]
   D Get a data-oriented stream API. arti#10 (1)
   o Make all APIs conform to best practices. arti#9 (1)
   o Figure out which consensus method we require, and say so. arti#8 (0.2) (45m)
   - Audit lifetime and locking code arti#14 (1)
       - Make a plan for what closes when
       - Make sure everything closes when it is supposed to.
       - What is locked?  What _could_ deadlock?  What prevents it?
   o Improve testing
       o complete^W 75% coverage on tor-proto arti#13 (2)
   o Refactor XXXX and TODO code; make sure everything is tested and
     documented.  (1-4??)
        o [arti#15 to make a list of these] (1h)
        o [arti#24 to resolve them]
   o expand documentation in tor-proto, tor-netdir, tor-netdoc,
     tor-cell. arti#16 (0.5)
   X Is this "reactor" business a sensible design?  Is there a better one?
     What else would a rustacean object to?  arti#17 (??)

MILESTONE A1: Dogfooding a socksport
   o Make connections on demand (1)
   o Build circuits on demand (2)
   o Downloading directory information, using compression
   - Support consensus diffs
   . coverage on important parts of netdir (0.5)
   o Optionally, expose a socks port (0.5)

MILESTONE A2: Experimental embedded client use
   - Minimal stable API. (2)
   - Begin using semver. (0.1)
   - Get a data-oriented stream API. arti#10 (3)

MILESTONE B: Secure minimal client
   . Correct path selection (2)
   - Timeouts
      . Circuit timeout logic (2??)
      . Connection timeout logic. (2??)
      - What other kinds of timeouts?
   - Connection padding (link protocol 5) (2)
   - Circuit padding (with padding machines) (4???)
   - Build preemptive circuits (2)
   - Guard nodes (3)
   - Change behavior depending on network parameters (3???)
   - CBT logic? (4???)
   - Pathbias logic (4???)
   . Figure out where to put a specific async executor and/or TLS
     implementation in our stack. (3???)

MILESTONE C: Client feature parity
   - V3 onion services (10)
   - Fairness on circuits/streams? (5??)
   - Support for using bridges (2??)
   - Pluggable transport support (5??)
   - Controller API? (10???)
   - Dormant mode? (2???)
   - Transparent proxy mode(s) (???)


MILESTONE H:
   - Be a hidden service

MILESTONE R: Relay support
   - Relay TLS handshake support
   - Directory cache support
   - Fairness support
   - Better circuit queues and circuitmuxes (for performance)
   - Statistics collection
   - Key management
   - Pluggable transports (server side)
   - Being a bridge
   - Self-testing
   - Publishing descriptors
   - Address discovery and configuration
   - DNS lookup
   - KIST scheduler
   - DoS-resistence handling
   - Rate-limiting
   - Accounting





- Spec issues
  - Test vectors
    . Add test vectors for ntor
    - Add test vectors for relay crypto
    - Add test vectors for hs-ntor
    - Add test vectors for hs-relay crypto
    - Add test vectors for TAP
  - Clarity
    - END cell format
  - Directory consistency
    - "-----BEGIN" should not be a valid keyword
    - Whitespace at start of line, y/n?  Mixed whitespace, y/n? CR, y/n?
    - UTF-8.


PROBABLY NEVER:
   - TAP
   - Link protocols before 4.
   - Older consensus methods


